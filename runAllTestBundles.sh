#! /bin/bash

# Copyright (C) 2013, Red Hat, Inc.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html

# Run Tycho Surefire on a set of Eclipse Test Bundles

# The definiton of an Eclipse Test Bundle for our purposes is any packaged
# OSGi bundle containing a pom.xml with a packaging type of
# 'eclipse-test-plugin'

# Takes a single argument (absolute path of folder containing test bundles)

if [ ! $# -eq 1 ]; then
  echo "USAGE : $0 PATH/TO/BUNDLES/DIRECTORY"
  exit 1
fi

testBundleFolder=$1

for jar in `find ${testBundleFolder} -name "*.jar"`; do
  jarPomPath=`jar -tf ${jar} | grep -E '(^pom.xml$|\/pom.xml$)'`
  unzip -p ${jar} ${jarPomPath} | grep -q '<packaging>eclipse-test-plugin</packaging>' && ./prepTestBundle.sh ${jar}
done

xmvn -o -fae org.eclipse.tycho:tycho-surefire-plugin:test
