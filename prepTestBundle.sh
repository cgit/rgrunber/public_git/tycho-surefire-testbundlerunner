#! /bin/bash

# Copyright (C) 2013, Red Hat, Inc.
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Public License v1.0
# which accompanies this distribution, and is available at
# http://www.eclipse.org/legal/epl-v10.html

# Support for running Tycho Surefire on an OSGi Test Bundle
# mvn org.eclipse.tycho:tycho-surefire-plugin:test

# Maven Conceptual Layout
# org.eclipse.tycho:org.eclipse.tycho.surefire.testbundlerunner
# | plugin:tycho-maven-plugin
# | plugin:target-platform-configuration
# | plugin:tycho-surefire-plugin (pluginManagement)
# |
# --> osgiTestBundle (gid:aid)
#       plugin:tycho-surefire-plugin (may inherit)


# File System Conceptual Layout
# org.eclipse.tycho:org.eclipse.tycho.surefire.testbundlerunner
# |
# --> pom.xml
# --> ${bsname}
#       |
#       --> pom.xml
#       --> META-INF/MANIFEST.MF
#       --> target/classes/**.class
#       --> resources/

function readBSN () {

bsn=
manEntryPat="^[a-zA-Z-]*:"
foundBSNLine=0

while read line; do
if [ ${foundBSNLine} -eq 1 ]; then
  echo ${line} | grep -qE ${manEntryPat}
  if [ $? -eq 0 ]; then
    break
  else
    bsn=${bsn}"`echo ${line} | sed 's/\([a-zA-Z0-9_.-]*\)\(;\)\?.*/\1/'`"
  fi
fi

echo ${line} | grep -q "Bundle-SymbolicName:"
if [ $? -eq 0 ]; then
  bsn=`echo ${line} | grep 'Bundle-SymbolicName:' | sed 's/Bundle-SymbolicName: \([a-zA-Z0-9_.-]*\)\(;\)\?.*/\1/'`
  echo ${line} | grep "Bundle-SymbolicName:" | grep -q ";"
  if [ $? -eq 0 ]; then
    break
  fi
  foundBSNLine=1
fi
done < <(unzip -p $1 'META-INF/MANIFEST.MF')

echo ${bsn}
}

if [ ! $# -eq 1 ]; then
  echo "USAGE : $0 /PATH/TO/JAR"
  exit 1
fi

jarLoc=$1

jarPomPath=`jar -tf ${jarLoc} | grep 'pom.xml'`
bsname=`readBSN ${jarLoc}`

mkdir ${bsname}
pushd ${bsname} >/dev/null

# place manifest and pom
jar -xf ${jarLoc} 'META-INF/MANIFEST.MF' ${jarPomPath}
mv ${jarPomPath} .
rm -rf 'META-INF/maven'

# set the parent
sed -i '/<relativePath>/ d' pom.xml
sed -i '/<parent>/,/<\/parent>/ s/<artifactId>.*<\/artifactId>/<artifactId>org.eclipse.tycho.surefire.testbundlerunner<\/artifactId>/ ' pom.xml
sed -i '/<parent>/,/<\/parent>/ s/<groupId>.*<\/groupId>/<groupId>org.eclipse.tycho<\/groupId>/ ' pom.xml
sed -i '/<parent>/,/<\/parent>/ s/<version>.*<\/version>/<version>1.0.0-SNAPSHOT<\/version>/ ' pom.xml

# place classes
# non-classes will just be ignored
mkdir -p target/classes
pushd target/classes >/dev/null
jar -xf ${jarLoc}
popd >/dev/null

# place resources and other files
# non-resources will just be ignored
jar -xf ${jarLoc}

popd >/dev/null

if [ ! -e pom.xml ]; then
  cp pom.xml{.orig,}
fi
# link the parent to the child
sed -i "/<\/modules>/ i <module>${bsname}<\/module>" pom.xml
